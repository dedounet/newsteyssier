--
# Des révélations! 
Par ici que ça se passe : https://dedounet.gitlab.io/newsteyssier/

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

