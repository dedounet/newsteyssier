+++
title = "Les News des Teyssier"
description = "Une chose importante à dire"
outputs = ["Reveal"]

[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.3
highlight_theme = "color-brewer"
transition = "slide"
transition_speed = "fast"
[reveal_hugo.templates.hotpink]
class = "hotpink"
background = "#FF4081"


+++

# 👏

Coucou, 
**merci d'avoir cliqué !**

*Il faut cliquer sur les symboles ou appuyer sur espace*

# <a href="#" class="navigate-right">➡️</a>

---

# Nous avons des choses à vous dire !

Car on se connaît depuis longtemps maintenant, n'est-ce pas ?

( non, Franck n'est pas **gay** et non Claire ne veut pas prendre la place de **Mélanchon** )


# <a href="#" class="navigate-right">➡️</a>

---

{{< slide background-color="#FF4081" >}}
# C'est BEAUCOUP PLUS {{% fragment %}} MIGNON ! {{% /fragment %}}


---

{{% section %}}
# Des changements nous ont permis de prendre conscience de beaucoup de choses

# <a href="#" class="navigate-down">⏬</a>

---

![chef chef caf](https://i.etsystatic.com/14783367/r/il/e2869f/1358826499/il_1140xN.1358826499_kvdk.jpg)

# <a href="#" class="navigate-down">⏬</a>
---

[![pas un haricot](https://sneakeradvisors.com/wp-content/uploads/2020/02/Logo-noir-sneakeradvisors.svg)](https://sneakeradvisors.com)

# <a href="#" class="navigate-down">⏬</a>

---
![bourg](https://as2.ftcdn.net/jpg/01/36/98/53/500_F_136985377_sev9MuqPH7fY4kJbgn92eanw8c8ihwVQ.jpg)

# <a href="#" class="navigate-down">⏬</a>

---

![tiflex](https://media-exp1.licdn.com/dms/image/C4D0BAQHhudN5Ffp5Sg/company-logo_200_200/0/1568044075879?e=2159024400&v=beta&t=ua_EsfrafV3J8XJ5RnLFvVydUnZRx0e7hk4h_GkCFtc)


# <a href="#" class="navigate-right">➡️</a>

{{% /section %}}

---

{{< slide background-color="#CECDCB" >}}
# Alors, quoi donc ? 
On s'est lancé dans la prospection et on a trouvé :

---
{{% section %}}

# En effet, ceci est une vue incroyable de l'intérieur de {{% fragment %}} **Claire** {{% /fragment %}}


# <a href="#" class="navigate-down">**👇**</a>

---

{{< slide transition="zoom" transition-speed="slow" >}}

![un haricot](images/a_baby.jpg)

# {{% fragment %}}  NOTRE BABY ! {{% /fragment %}}

{{% /section %}}

---

# Voila, pleins de bisous les copains !

 *(Oui, c'est cliché de vous montrer la photo de l'echographie mais on a pas trouvé mieux...)*  

 *(Et on est trop fier!)* 




